# CAPTION: Correction by Analyses, POS -Tagging and Interpretation of Objects using only Nouns

This repository provides the code used for the CAPTION paper presented in [IMA2019](https://sites.google.com/view/ima2019).  

The directory structure needed to run the code is provided, but data from TensorFlow model zoo, FOIL-COCO and MS-COCO are not provided in this repository. Thus, it is necessary to download each one of them inside the `data` folder.

