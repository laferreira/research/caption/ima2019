FROM tensorflow/tensorflow:1.14.0-gpu-py3-jupyter

RUN apt-get update && \
    apt-get install graphviz wget git -y && \
    apt-get autoremove && \
    apt-get clean && \
    apt-get autoclean

RUN pip3 install --no-cache-dir \
    seaborn \
    sklearn \
    pydot \
    requests \
    jupyterlab \
    opencv-python \
    git+https://github.com/douglasrizzo/dodo_detector.git \
    nltk \
    actdiag blockdiag nwdiag seqdiag

RUN wget -O protobuf.zip https://github.com/google/protobuf/releases/download/v3.0.0/protoc-3.0.0-linux-x86_64.zip && \
    unzip protobuf.zip && \
    mv bin/protoc /bin

CMD ["bash", "-c", "source /etc/bash.bashrc && export PYTHONPATH=$PYTHONPATH:/notebooks/data/tensorflow/models/research/:/notebooks/data/tensorflow/models/research/slim && jupyter lab --allow-root --ip 0.0.0.0"]
