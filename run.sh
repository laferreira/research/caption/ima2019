#!/bin/bash

docker run --rm \
       --gpus all \
       -p 8888:8888 \
       --mount type=bind,source=$PWD/src/jupyter,destination=/notebooks\
       --mount type=bind,source=$PWD/figs,destination=/notebooks/figs \
       --mount type=bind,source=$HOME/files,destination=/notebooks/data\
       --name caption \
       caption
