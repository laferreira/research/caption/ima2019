from __future__ import absolute_import, division, print_function, unicode_literals
from os import listdir
from pickle import dump

from numpy import array
from PIL import Image

from paths import model, inference_graph_path, label_map_path
from paths import coco_train, coco_test, coco_val

from dodo_detector.detection import SingleShotDetector
detector = SingleShotDetector(inference_graph_path, label_map_path)

# --------------------------------------------------------------------------- #
def get_objects(image_path):
    img = array(Image.open(image_path))
    _, objs = detector.from_image(img)
    
    return objs
    
# --------------------------------------------------------------------------- #

import tensorflow as tf
n_gpu = len(tf.config.experimental.list_physical_devices('GPU'))

print("N. GPUS:\t{}".format(n_gpu))

if n_gpu > 0:
    train = [coco_train + '/' + file for file in listdir(coco_train)]
    test = [coco_test + '/' + file for file in listdir(coco_test)]
    val = [coco_val + '/' + file for file in listdir(coco_val)]

    images = train + test + val

    detection = dict()
        
    total = len(images)

    for i, image in enumerate(images):
        print("Image {} of {}".format(i+1, total))
            
        img_file = image.split('/')[-1]
        objs = get_objects(image)
        detection[img_file] = objs

        if i % 50 == 0:
            print("Saving detections")
            with open(model+'.pck','wb') as file:
                dump(detection, file)

    print("Saving detections")
    with open(model+'.pck','wb') as file:
        dump(detection, file)

else:
    print("NO GPU FOUND!!!")

# --------------------------------------------------------------------------- #