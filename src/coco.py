class Detector:

    from numpy import array
    from PIL import Image
    
# --------------------------------------------------------------------------- # 
    def __init__(self, inference_graph, label_map, images_path, confidence):
        from dodo_detector.detection import SingleShotDetector
        
        self.detector = SingleShotDetector(inference_graph, label_map, confidence=confidence)
        self.imgs_path = images_path
        
# --------------------------------------------------------------------------- #
    def get_img_and_objs(self, image):
        
        img = self.array(self.Image.open(self.imgs_path + '/' + image))
        marked_img, objs_info = self.detector.from_image(img)
        objs = list(objs_info.keys())
        return objs, marked_img, objs_info
    
# --------------------------------------------------------------------------- #