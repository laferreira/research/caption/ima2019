# --------------------------------------------------------------------------- #
# included libraries
from json import dump
# --------------------------------------------------------------------------- #
# from the folder
from paths import *
from foil import Foil
from foil2coco import Foil2Coco
from coco import Detector
# --------------------------------------------------------------------------- #

# --------------------------------------------------------------------------- #
foil = Foil(foil_test)
f2c = Foil2Coco()
detector = Detector(inference_graph_path, label_map_path, coco_train, 0.5)

dataset = dict()
similarity = 'path'
dataset_len = len(foil.data)
for i, data in enumerate(foil.data):
    print("Line {} of {}".format(i+1, dataset_len))
    try:
        img = foil.get_foil_img(data['image_id'])
        objs, marked_img, _ = detector.get_img_and_objs(img)
        classes = [f2c.get_supercategory(obj, 'lch') for obj in objs]

        caption = data['caption']
        nouns_names_scateg = f2c.get_coco_caption(caption, similarity)

        dataset[i+1] = {'image_id': img,
                      'foil-error': data['foil_word'],
                      'foil-correction': data['target_word'],
                      'image': objs,
                      'caption': [names for _,names,_
                                  in nouns_names_scateg],
                      'capt_scateg': [scateg for _,_,scateg
                                      in nouns_names_scateg],
                      'capt_nouns': [nouns for nouns,_,_
                                     in nouns_names_scateg]
                     }

    except FileNotFoundError:
        print("Image {} not found".format(img))
        
    if i % 50 == 0:
        print("Saving...", end="")
        with open('dataset.json','w') as file:
            dump(dataset, file)
        print("done!")
            
with open('dataset.json','w') as file:
    dump(dataset, file)
# --------------------------------------------------------------------------- #
