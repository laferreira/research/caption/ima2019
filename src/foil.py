class Foil:
    
# --------------------------------------------------------------------------- #
    def __init__(self, path):
        import json
        
        with open(path) as file:
            self.data = json.load(file)['annotations']
            
# --------------------------------------------------------------------------- #
    def get_foil_img(self, img_id):
        
        return (str(img_id) + '.jpg').rjust(16,'0')
    
# --------------------------------------------------------------------------- #
