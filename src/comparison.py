class Comparison:
    
# --------------------------------------------------------------------------- #
    def __init__(self, json_path):
        from json import load

        with open(json_path,'r') as file:
            self.dataset = load(file)
            
        with open('data/foil/foilv1.0_test_2017.json', 'r') as file:
            self.total_entries = len(load(file)['annotations'])
            
        with open('categories.json','r') as file:
            self.names = {c['name']:c['supercategory'] for c in load(file)}
            
# --------------------------------------------------------------------------- #
    def found_in_both(self, caption, image):
        return set(caption).intersection(set(image))
    
# --------------------------------------------------------------------------- #
    def caption_only(self, caption, image):
        return set(caption) - set(image)
    
# --------------------------------------------------------------------------- #
    def image_only(self, caption, image):
        return set(image) - set(caption)
    
# --------------------------------------------------------------------------- #
    def compare(self):

        for i, data in self.dataset.items():
            both = self.found_in_both(data['caption'], data['image'])
            caption_only = self.caption_only(data['caption'], data['image'])
            image_only = self.image_only(data['caption'], data['image'])
            
            data['in_both'] = both
            data['caption_only'] = caption_only
            data['image_only'] = image_only
            
            foil, correction = self.caption_is_foil(data)
            data['foil'] = foil
            data['correction'] = correction
            if foil:
                data['found_word'] = data['foil-error'] in correction
            
# --------------------------------------------------------------------------- #
    def statistics(self):
        
        self.compare()
        data = self.dataset
        
        error_found = len([v for v in data.values() 
                           if v['foil'] and v['foil-error'] != 'ORIG'])
        
        error_total = len([v for v in data.values() 
                           if not v['foil-error'] == 'ORIG'])
        
        correct_found = len([v for v in data.values() 
                             if not v['foil'] and v['foil-error'] == 'ORIG'])
        
        correct_total = len([v for v in data.values() 
                             if v['foil-error'] == 'ORIG'])
        
        found_word = len([v for v in data.values() 
                          if 'found_word' in v and v['found_word']])

        total = len(data)
        
        found_total = error_found + correct_found
        
        output = "Total classified entries:\t{:6d}/{:6d} ({:>6.2f}%)\n".format(
                 total, self.total_entries, (total/self.total_entries)*100)
        
        output += "Correct classifications:\t{:6d}/{:6d} ({:>6.2f}%)\n".format(
                 found_total, total, (found_total/total)*100)
        
        output += "Not foil classification:\t{:6d}/{:6d} ({:>6.2f}%)\n".format(
                  correct_found, correct_total,
                  (correct_found/correct_total)*100)
        
        output += "Foil classification:\t\t{:6d}/{:6d} ({:>6.2f}%)\n".format(
                  error_found, error_total, (error_found/error_total)*100)
        
        output += "Wrong words found:\t\t{:6d}/{:6d} ({:>6.2f}%)\n".format(
                  found_word, error_found, (found_word/error_found)*100)
        
        return output
        
# --------------------------------------------------------------------------- #
    def save_dataset(self, path):
        from json import dump
        
        with open(path, 'w') as file:
            dump(self.dataset, file)
            
# --------------------------------------------------------------------------- #
    def get_Y_pred_and_test(self):
        
        is_foil = [i['foil-error'] != 'ORIG' for i in self.dataset.values()]
        foil_found = [i['foil'] for i in self.dataset.values()]
        
        return foil_found, is_foil
    
# --------------------------------------------------------------------------- #
    def results(self):
        from sklearn.metrics import classification_report as report
        
        output = "-"*80 + "\n"
        output += self.statistics()
        output += "-"*80 + "\n"
        output += report(*self.get_Y_pred_and_test(),
                         target_names=['Correct','FOIL'])
        output += "-"*80 + "\n"
        
        return output
# --------------------------------------------------------------------------- #
    def get_supercategory(self, word):
        return self.names[word]
    
# --------------------------------------------------------------------------- #
    def caption_is_foil(self, data): 
        
        foil = False
        error = list()
        correction = list()
        
        caption = data['caption_only']
        
        if caption:        
            image = data['image_only']
            image_n_sc = {word: self.get_supercategory(word) 
                          for word in image}
            caption_n_sc = {word: self.get_supercategory(word) 
                            for word in caption}
            image_sc = set(image_n_sc.values())
            
            error =[word for word, scateg in caption_n_sc.items() 
                    if scateg in image_sc]
            
            correction = {e: [word for word, scateg in image_n_sc.items()
                                if scateg == caption_n_sc[e]] 
                          for e in error}
            
            if correction: 
                foil = True
            
        return foil, correction
    
# --------------------------------------------------------------------------- #