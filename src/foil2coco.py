¡class Foil2Coco:
    from operator import itemgetter
    from itertools import product
    
    from nltk import download

# --------------------------------------------------------------------------- #
    # relate synsets
    download('wordnet')
    
    # find nouns in sentences
    download('punkt')
    download('averaged_perceptron_tagger')
    
    from nltk.corpus import wordnet as wn
    
# --------------------------------------------------------------------------- #
    def __init__(self):
        import json

        with open('categories.json','r') as file:
            names = {c['name']:c['supercategory'] for c in json.load(file)}
            
        self.names = names
        
        self.coco = {n:self.get_synsets(n) for n in self.names.keys()}
        
        self.supercategories = {n:self.get_synsets(n)
                                for n in self.names.values()}
        
        self.sc2names = {sc:[n for n,s in self.names.items() if s == sc] 
                         for sc in self.supercategories}
            
        self.sim_dict = {'wup': self.wn.wup_similarity,
                         'path': self.wn.path_similarity,
                         'lch': self.wn.lch_similarity}

# --------------------------------------------------------------------------- #
    def get_synsets(self, word, pos='n'):
        
        synsets = self.wn.synsets(word, pos='n')
        word_synset = self.product([word], synsets)
        filter_fn = lambda w_s: w_s[0] == w_s[1].name().split('.')[0]

        return [w_s[1] for w_s in filter(filter_fn, word_synset)]
    
# --------------------------------------------------------------------------- #
    def apply_fn_to_synsets_prods(self, word, categories, fn):
        
        noun_wn = self.get_synsets(word)
        
        return {name:[fn(noun_synset, name_synset) 
                      for noun_synset, name_synset in 
                      self.product(noun_wn, categories[name])]
                for name in categories}
    
# --------------------------------------------------------------------------- #
    def get_similarity_list(self, word, categories, sim):
        
        similarity_fn = self.sim_dict[sim]
        
        return self.apply_fn_to_synsets_prods(word, categories, similarity_fn)

# --------------------------------------------------------------------------- #
    def get_similarity_for_categories(self, categories):
        
        for name, similarities in categories.items():
            if not similarities:
                categories[name] = 0
            else:
                categories[name] = max([i for i in similarities 
                                        if i is not None])
        return categories
    
# --------------------------------------------------------------------------- #
    def get_most_similar(self, word, categories, sim):
        
        names_sims = self.get_similarity_list(word, categories, sim)
        names_sims = self.get_similarity_for_categories(names_sims)
        return max(names_sims.items(), key=self.itemgetter(1))[0]
        
# --------------------------------------------------------------------------- #
    def get_coco_name(self, word, sim):
        
        return self.get_most_similar(word, self.coco, sim)
    
# --------------------------------------------------------------------------- #
    def get_supercategory(self, word, sim):
        
        
        return self.names[word] if word in self.names \
               else self.get_most_similar(word, self.supercategories, sim)
    
# --------------------------------------------------------------------------- #
    def get_coco_caption(self, caption, sim):
        from nltk import word_tokenize, pos_tag
        
        tag_words = lambda caption: pos_tag(word_tokenize(caption))
        
        nouns = [word for word,tag in  tag_words(caption) if tag[0] =='N']
        #cap2coco = [self.get_coco_name(word, sim) for word in nouns]
        #nouns_categ = [self.get_supercategory(word, sim) for word in cap2coco]
        
        name_scateg = [(noun, *self.get_name_from_supercategory(noun, sim)) 
                   for noun in nouns]
        
        #return cap2coco, nouns_categ, nouns
        return name_scateg

# --------------------------------------------------------------------------- #
    def get_name_from_supercategory(self, word, sim):
        
        scateg = self.get_supercategory(word, sim)
    
        synsets = {name: synset for name, synset in self.coco.items() 
                   if name in self.sc2names[scateg]}
        
        return self.get_most_similar(word, synsets, sim), scateg
    
# --------------------------------------------------------------------------- #